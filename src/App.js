import { Route, Switch } from "react-router-dom";
import Heading from "./components/shared/Heading";
import PostList from "./components/layers/PostList";
import PostForm from "./components/layers/PostForm";
import { ContextProvider } from "./context/GlobalContext";
import "./App.css";
import Footer from "./components/shared/Footer";
import PostComments from "./components/layers/PostComments";

function App() {
  return (
    <div>
      <Heading />
      <div className="text-center p-10">
        <div className="container mx-auto h-full">
          <ContextProvider>
            <Switch>
              <Route path="/" component={PostList} exact />
              <Route path="/add" component={PostForm} />
              <Route path="/post/:id" component={PostComments} exact />
            </Switch>
          </ContextProvider>
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default App;
