import React from "react";
import { AiOutlineDown } from "react-icons/ai";
const cls =
  "bg-indigo-700 text-white font-semibold py-2 px-4 rounded inline-flex items-center";

const Button = ({ onClick }) => (
  <button className={cls} onClick={onClick}>
    &nbsp;
    <AiOutlineDown />
    &nbsp;
  </button>
);

export default Button;
