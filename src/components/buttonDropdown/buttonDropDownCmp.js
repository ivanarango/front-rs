import React, { useEffect } from "react";
import Button from "./button";
import DropDownCard from "./dropDownCard";
const sampleData = ["Mi perfil", "Mensajes", "Grupos"];
const ButtonWithDropDownCmp = () => {
  const [open, setOpen] = React.useState(false);
  const drop = React.useRef(null);
  function handleClick(e) {
    if (!e.target.closest(`.${drop.current.className}`) && open) {
      setOpen(false);
    }
  }
  useEffect(() => {
    document.addEventListener("click", handleClick);
    return () => {
      document.removeEventListener("click", handleClick);
    };
  });
  return (
    <div className="dropdown" ref={drop}>
      <Button onClick={() => setOpen((open) => !open)} />
      {open && <DropDownCard data={sampleData} setOpen={setOpen} />}
    </div>
  );
};

export default ButtonWithDropDownCmp;
