import React, { useContext, useEffect } from "react";
import { GlobalContext } from "../../context/GlobalContext";
import { useParams } from "react-router-dom";
import Post from "../shared/Post";
import Comment from "../shared/Comment";
import CommentForm from "../shared/CommentForm";
import { BiArrowBack } from "react-icons/bi";
import { Link } from "react-router-dom";
const PostComments = () => {
  const { posts } = useContext(GlobalContext);
  const params = useParams();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  let post = posts.find((post) => post.id === parseInt(params.id));

  return (
    post !== undefined && (
      <div>
        <div className="text-left">
          <Link to={`/`}>
            <button className="bg-red-400 hover:bg-red-500 text-white font-semibold py-2 px-4 rounded inline-flex items-center">
              <BiArrowBack />
              &nbsp;Atras
            </button>
          </Link>
        </div>
        <div className="flex justify-center items-center h3-4 mb-10">
          <div className="w-6/12">
            <Post post={post} />
            <CommentForm idPost={post.id} />
            {post.comments.map((comment) => (
              <Comment key={comment.id} comment={comment} />
            ))}
          </div>
        </div>
      </div>
    )
  );
};

export default PostComments;
