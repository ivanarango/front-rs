import React, { useContext } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
import Constants from "../../constants/constants";
import { GlobalContext } from "../../context/GlobalContext";
import { useForm } from "react-hook-form";

const PostForm = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const history = useHistory();
  const { cleanPosts } = useContext(GlobalContext);

  const addNewPost = (post) => {
    axios
      .post(`${Constants.URL_API}/post`, post)
      .then(() => {
        cleanPosts();
        history.push("/");
      })
      .catch(() => {
        alert("error de comunicacion con el backend");
      });
  };

  const onsubmit = (post) => {
    addNewPost(post);
  };

  return (
    <div className="flex justify-center items-center h3-4">
      <form
        className="border-md shadow-md rounded-lg p-10"
        onSubmit={handleSubmit(onsubmit)}
      >
        <div className="bg-gray-200 py-2 px-4 rounded-md shadow-2x1 font-bold mb-5">
          <div className="bg-blue-500 py-2 px-4 rounded-full shadow-2x1 font-bold inline-flex mb-5">
            <h2 className="text-center text-white font-semibold text-3x1">
              Nuevo Post
            </h2>
          </div>
          <div className="mb-5">
            <label className="font-semibold text-3x1">Nombre del post</label>
            <input
              {...register("postName", {
                required: {
                  value: true,
                  message: "El nombre del Post es un campo obligatorio",
                },
                maxlength: {
                  value: 50,
                  message:
                    "El nombre del post debe ser de maximo 50 caracteres",
                },
              })}
              type="text"
              name="postName"
              className="rounded-md mt-2 py-3 px-4 bg-white w-full"
            />
            <p className="text-red-500 text-xs italic">
              {errors.postName && errors.postName.message}
            </p>
          </div>
          <div className="mb-5">
            <label className="font-semibold text-3x1">
              Autor (correo de contacto)
            </label>
            <input
              {...register("emailCreator", {
                required: {
                  value: true,
                  message: "El correo del autor es un campo obligatorio",
                },
                pattern: {
                  value: /^\S+@\S+$/,
                  message: "El correo debe ser valido",
                },
              })}
              type="text"
              name="emailCreator"
              className="rounded-md py-3 px-4 bg-white w-full"
            />
            <p className="text-red-500 text-xs italic">
              {errors.emailCreator && errors.emailCreator.message}
            </p>
          </div>

          <div className="mb-5">
            <label className="font-semibold text-3x1">Contenido del post</label>
            <textarea
              {...register("content", {
                required: {
                  value: true,
                  message: "El contenido del Post es un campo obligatorio",
                },
                maxlength: {
                  value: 200,
                  message:
                    "El contenido del post debe ser de maximo 200 caracteres",
                },
              })}
              name="content"
              rows="2"
              className="rounded-md py-3 px-4 bg-white w-full"
            ></textarea>
            <p className="text-red-500 text-xs italic">
              {errors.content && errors.content.message}
            </p>
          </div>
        </div>
        <button className="bg-green-400 hover:bg-green-500 text-white font-semibold py-2 px-4 rounded inline-flex items-center">
          Publicar
        </button>
      </form>
    </div>
  );
};

export default PostForm;
