import axios from "axios";
import React, { useContext, useEffect, useState , useRef} from "react";
import Constants from "../../constants/constants";
import { GlobalContext } from "../../context/GlobalContext";
import Post from "../shared/Post";
const PostList = () => {
  const { posts, scrollPosition } = useContext(GlobalContext);
  const { addManyPosts } = useContext(GlobalContext);
  const total = useRef(0);
  const [loading, setLoading] = useState(false);
  const firstRender1 = useRef(true);
  const [morePosts,setMorePosts] = useState(false);

  useEffect(() => {
    async function getPosts() {
      setLoading(true);
      const res = await axios.get(`${Constants.URL_API}/post`, {
        params: {
          take: 10,
          skip: posts.length,
          page: 0,
        },
      });
      total.current = res.data.data.paginationResponse.total;
      if (res.data.data.posts.length > 0) {
        addManyPosts(res.data.data.posts);
      }
      setLoading(false);
    }

    if (firstRender1.current){
      firstRender1.current = !firstRender1.current;
      window.scrollTo(0, scrollPosition);
    } 
    getPosts();
  }, [morePosts]);

  window.onscroll = function () {
    let d = document.documentElement;
    let offset = d.scrollTop + window.innerHeight;
    let height = d.offsetHeight;
    if (offset >= height) {
      setMorePosts(!morePosts);
    }
  };
  const Loader = () => {
    return (
      <div className=" flex justify-center items-center">
        <div className="animate-spin rounded-full h-32 w-32 border-t-2 border-b-2 border-blue-500"></div>
      </div>
    );
  };

  return (
    <div className="flex justify-center items-center h3-4 mb-10">
      <div className="w-6/12">
        {posts.map((post) => (
          <Post key={post.id} post={post} />
        ))}
        {loading && Loader()}
      </div>
    </div>
  );
};

export default PostList;
