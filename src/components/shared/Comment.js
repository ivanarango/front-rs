const Comment = (props) => {
  const { comment } = props;
  return (
    <div className="bg-white border-2 rounded-lg p-2">
      <div className="border-b-2 py-2">
        <div className="pl-3">
          <div className="font-medium">{comment.commentName}</div>
          <div className="text-gray-600 text-sm">{comment.emailCreator}</div>
        </div>
      </div>
      <div>
        <p className="text-gray-400 leading-loose font-normal text-base">
          {new Date(comment.createdDate).toLocaleString()}
        </p>
      </div>
      <div>
        <p className="text-gray-900 leading-loose font-normal text-base">
          {comment.content}
        </p>
      </div>
    </div>
  );
};

export default Comment;
