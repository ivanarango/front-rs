import { useContext } from "react";
import axios from "axios";
import Constants from "../../constants/constants";
import { GlobalContext } from "../../context/GlobalContext";
import { useForm } from "react-hook-form";
const CommentForm = (props) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const { idPost } = props;
  const { modifyPostComments } = useContext(GlobalContext);

  const onsubmit = (comment,e) => {
    comment.post = idPost;
    addNewComment(comment);
    e.target.reset();
  };

  const addNewComment = (comment) => {
    axios
      .post(`${Constants.URL_API}/comment`, comment)
      .then((res) => modifyPostComments(res.data.data, idPost))
      .catch();
  };

  return (
    <div className="text-gray-700 px-6 py-4 border-t border-gray-200 border rounded-lg p-4 bg-gray-100 space-x-10">
      <form onSubmit={handleSubmit(onsubmit)}>
        <div className="mb-5">
          <label className="font-semibold text-3x1">
            Nombre del comentario
          </label>
          <input
            {...register("commentName", {
              required: {
                value: true,
                message: "El nombre del comentario es un campo obligatorio",
              },
              maxlength: {
                value: 50,
                message:
                  "El nombre del comentario debe ser de maximo 50 caracteres",
              },
            })}
            type="text"
            name="commentName"
            className="rounded-md mt-2 py-3 px-4 bg-white w-full"
          />
          <p className="text-red-500 text-xs italic">
            {errors.commentName && errors.commentName.message}
          </p>
        </div>
        <div className="mb-5">
          <label className="font-semibold text-3x1">
            Contenido del comentario
          </label>
          <textarea
            {...register("content", {
              required: {
                value: true,
                message: "El contenido del comentario es un campo obligatorio",
              },
              maxlength: {
                value: 200,
                message:
                  "El contenido del comentario debe ser de maximo 100 caracteres",
              },
            })}
            name="content"
            rows="2"
            className="rounded-md py-3 px-4 bg-white w-full"
          ></textarea>
          <p className="text-red-500 text-xs italic">
            {errors.content && errors.content.message}
          </p>
        </div>
        <div className="mb-5">
          <label className="font-semibold text-3x1">
            Autor (correo de contacto)
          </label>
          <input
            {...register("emailCreator", {
              required: {
                value: true,
                message: "El correo del autor es un campo obligatorio",
              },
              pattern: {
                value: /^\S+@\S+$/,
                message: "El correo debe ser valido",
              },
            })}
            type="text"
            name="emailCreator"
            className="rounded-md py-3 px-4 bg-white w-full"
          />
          <p className="text-red-500 text-xs italic">
            {errors.emailCreator && errors.emailCreator.message}
          </p>
        </div>
        <button className="bg-green-400 hover:bg-green-500 text-white font-semibold py-2 px-4 rounded">
          Comentar
        </button>
      </form>
    </div>
  );
};

export default CommentForm;
