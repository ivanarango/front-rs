import React from "react";

const Footer = () => {
  return (
    <div className="bg-blue-500 text-center text-white font-bold text-2xl p-2 fixed bottom-0 w-full">
      <p>Vecindario &copy; {new Date().getFullYear()}</p>
    </div>
  );
};

export default Footer;
