import React from "react";
import { Link } from "react-router-dom";
import { MdOutlinePostAdd } from "react-icons/md";
import ButtonWithDropDownCmp from "../buttonDropdown/buttonDropDownCmp";

const Heading = () => {
  return (
    <div className="sticky top-0 z-50">
      <div className="bg-blue-500 flex item-center">
        <div className="py-5 px-4">
          <Link to="/">
            <div className="text-white font-bold text-2x1">
              Vecindario Social
            </div>
          </Link>
        </div>
        <div className="text-right px-4 py-1 m-2 inline-flex right-0 absolute space-x-4">
          <>
            <ButtonWithDropDownCmp />
          </>

          <Link to="/add">
            <button className="bg-green-400 hover:bg-green-500 text-white font-semibold py-2 px-4 rounded inline-flex items-center">
              <MdOutlinePostAdd size={20} />
              Nuevo post
            </button>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Heading;
