import axios from "axios";
import React, { useEffect, useRef, useState, useContext } from "react";
import { BiDislike, BiLike } from "react-icons/bi";
import { CgComment } from "react-icons/cg";
import { Link } from "react-router-dom";
import Constants from "../../constants/constants";
import { GlobalContext } from "../../context/GlobalContext";

const Post = (props) => {
  const { post } = props;
  const { postLiked, postDisliked, modifyPostReactions, saveScrollFeed } =
    useContext(GlobalContext);
  const firstRender1 = useRef(true);
  const firstRender2 = useRef(true);
  const [stateLikeButton, setStateLikeButton] = useState(
    postLiked.includes(post.id)
  );
  const [stateDislikeButton, setStateDislikeButton] = useState(
    postDisliked.includes(post.id)
  );
  useEffect(() => {
    if (firstRender1.current) firstRender1.current = !firstRender1.current;
    else buildReaction(true);
  }, [stateLikeButton]);

  useEffect(() => {
    if (firstRender2.current) firstRender2.current = !firstRender2.current;
    else buildReaction(false);
  }, [stateDislikeButton]);

  async function buildReaction(isLike) {
    if (isLike) {
      if (stateLikeButton) {
        sendReaction("+", "LIKE");
      } else {
        sendReaction("-", "LIKE");
      }
    } else {
      if (stateDislikeButton) {
        sendReaction("+", "DISLIKE");
      } else {
        sendReaction("-", "DISLIKE");
      }
    }
  }

  function sendReaction(action, type) {
    axios
      .put(`${Constants.URL_API}/post/${post.id}`, {
        action,
        type,
      })
      .then(() => {
        modifyPostReactions(action, type, post.id);
      });
  }

  function saveScroll() {
    saveScrollFeed(window.scrollY);
  }

  return (
    <div className="bg-white shadow-lg rounded-lg mb-3">
      <Link to={`/post/${post.id}`} onClick={saveScroll}>
        <div className="bg-blue-400 text-gray-700 text-lg font-bold px-6 py-4">
          <p>{post.postName}</p>
        </div>
      </Link>
      <div className="px-6 py-4 border-t border-gray-200">
        <div className="text-justify border rounded-lg p-4 bg-gray-100 text-gray-700 font-semibold">
          {post.content}
        </div>
      </div>
      <div className="flex justify-between items-center px-6 py-4">
        <div className="bg-green-600 text-xs px-2 py-1 rounded-full border border-gray-200 text-gray-200 font-bold">
          {post.emailCreator}
        </div>
        <div className="text-black text-sm">
          Publicado el: {new Date(post.createdDate).toLocaleString()}
        </div>
      </div>

      <div className="text-gray-700 px-6 py-4 border-t border-gray-200 border rounded-lg p-4 bg-gray-100 space-x-10">
        <button
          onClick={() => setStateLikeButton(!stateLikeButton)}
          className={
            stateLikeButton
              ? "hover:bg-transparent bg-green-500 hover:text-green-700 font-semibold  text-white py-2 px-4 border hover:border-green-600 border-transparent rounded"
              : "bg-transparent hover:bg-green-500 text-green-700 font-semibold  hover:text-white py-2 px-4 border border-green-600 hover:border-transparent rounded"
          }
        >
          <BiLike /> {props.post.likes}
        </button>
        <button
          onClick={() => setStateDislikeButton(!stateDislikeButton)}
          className={
            stateDislikeButton
              ? "hover:bg-transparent bg-red-500 hover:text-red-700 font-semibold text-white py-2 px-4 border hover:border-red-600 border-transparent rounded"
              : "bg-transparent hover:bg-red-500 text-red-700 font-semibold hover:text-white py-2 px-4 border border-red-600 hover:border-transparent rounded"
          }
        >
          <BiDislike /> {post.dislikes}
        </button>
        <Link to={`/post/${post.id}`} onClick={saveScroll}>
          <button className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-600 hover:border-transparent rounded">
            <CgComment /> {props.post.comments.length}
          </button>
        </Link>
      </div>
    </div>
  );
};

export default Post;
