export default function appReducer(state, request) {
  switch (request.type) {
    case "ADD_POST":
      return { ...state, posts: [request.payload, ...state.posts] };
    case "ADD_MANY_POSTS":
      return { ...state, posts: [...state.posts, ...request.payload] };
    case "MODIFY_POST_REACTION":
      const idPostReactioned = request.payload.idPostReactioned;
      if (request.payload.reaction === "LIKE") {
        if (request.payload.action === "+") {
          return {
            ...state,
            posts: state.posts.map((post) =>
              post.id === idPostReactioned
                ? { ...post, likes: post.likes + 1 }
                : post
            ),
            postLiked: [...state.postLiked, idPostReactioned],
          };
        } else if (request.payload.action === "-") {
          return {
            ...state,
            posts: state.posts.map((post) =>
              post.id === idPostReactioned
                ? { ...post, likes: post.likes - 1 }
                : post
            ),
            postLiked: state.postLiked.filter(
              (idPost) => idPost !== idPostReactioned
            ),
          };
        }
      }
      if (request.payload.reaction === "DISLIKE") {
        if (request.payload.action === "+") {
          return {
            ...state,
            posts: state.posts.map((post) =>
              post.id === idPostReactioned
                ? { ...post, dislikes: post.dislikes + 1 }
                : post
            ),
            postDisliked: [...state.postDisliked, idPostReactioned],
          };
        } else if (request.payload.action === "-") {
          return {
            ...state,
            posts: state.posts.map((post) =>
              post.id === idPostReactioned
                ? { ...post, dislikes: post.dislikes - 1 }
                : post
            ),
            postDisliked: state.postDisliked.filter(
              (idPost) => idPost !== idPostReactioned
            ),
          };
        }
      }
      return state;
    case "MODIFY_POST_COMMENT":
      return {
        ...state,
        posts: state.posts.map((post) =>
          post.id === request.payload.idPost
            ? { ...post, comments: [...post.comments, request.payload.comment] }
            : post
        ),
      };
    case "SAVE_SCROLL_FEED":
      return { ...state, scrollPosition: request.payload.position };
    case "CLEAN_POSTS":
      return { ...state, posts: [] };
    default:
      break;
  }
}
