import { createContext, useReducer } from "react";
import appReducer from "./AppReducer";

const initialState = {
  posts: [],
  postLiked: [],
  postDisliked: [],
  scrollPosition: 0,
};

export const GlobalContext = createContext(initialState);

export const ContextProvider = ({ children }) => {
  const [state, dispatch] = useReducer(appReducer, initialState);

  const addPost = (post) => {
    dispatch({
      type: "ADD_POST",
      payload: { ...post },
    });
  };

  const addManyPosts = (posts) => {
    dispatch({ type: "ADD_MANY_POSTS", payload: posts });
  };

  const cleanPosts = () => {
    dispatch({
      type: "CLEAN_POSTS"
    });
  }

  const modifyPostReactions = (action, reaction, idPostReactioned) => {
    dispatch({
      type: "MODIFY_POST_REACTION",
      payload: {
        action,
        reaction,
        idPostReactioned,
      },
    });
  };

  const modifyPostComments = (comment, idPost) => {
    dispatch({
      type: "MODIFY_POST_COMMENT",
      payload: {
        comment,
        idPost,
      },
    });
  };

  const saveScrollFeed = (position) => {
    dispatch({
      type: "SAVE_SCROLL_FEED",
      payload: {
        position,
      },
    });
  };
  return (
    <GlobalContext.Provider
      value={{
        ...state,
        addPost,
        addManyPosts,
        modifyPostReactions,
        modifyPostComments,
        saveScrollFeed,
        cleanPosts
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
};
